FROM cypress/base

ARG NODE_VERSION='18.19.0'

RUN mkdir /app
WORKDIR /app
COPY package.json /app

RUN apt update && apt install -y chromium
RUN npm install --save-dev cypress

RUN npm install

COPY . /app







