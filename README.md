# London Voice e2e tests
## Description
This project is an automated test script written with cypress to test features of londongpt.vercel.app/ using the ethers library initialized when tests are run to bypass testing with external applications such as metamask.

## Before run
In the main folder, add file `cypress.env.json` and put an object with wallet private key and mail slurp api key (see `cypress.env.example` to compare).
The private key is needed to initialize the ethers wallet during test initialization. 

To receive the mailslurp api key, you need to create a free account on the mailslurp website. https://www.mailslurp.com/examples/cypress-js/

## Installation & run
Install project dependencies:

    npm i
    
To be able to test the application, it is necessary to first perform a minting test in order to receive permission to perform the rest of the tests:

    npm run cypress:london-voice-minting

To test login with e-mail, run:

    npm run cypress:london-voice-mail-login

To run the rest of the test, use:

    npm run london-voice-tests


## Reporting
The following tests have the implementation of reporters that allow you to save test results in HTML form. To create application tests (excluding minting and email login tests), follow these steps:

    npm run prereport                       #delete any existing reports
    npm run cypress:london-voice-tests
    npm run postreport                      #generate html report

Results are saved as `mochawesome_combined.html` in `./mochawesome-report`. 

## Docker
To run tests using Docker:

    docker compose --profile frontend up

Reports are saved as `mochawesome_combined.html` in `./dockerReports`. 