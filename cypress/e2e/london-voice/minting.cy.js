const {
  pageElements,
  iFrameElements,
} = require("../../pages/london-voice/mainPage");
const {
  mintingModulePopUp,
  postMintScreen,
} = require("../../pages/london-voice/mintingModal");

describe("Mint", () => {
  beforeEach("", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
  });

  it("Check mint and minting page ", () => {
    cy.task("getQRCodes").then((qrCode) => {
      cy.visit("/");
      cy.visit(qrCode);

      //content and style checks
      cy.get(mintingModulePopUp.mintingModalText).should(
        "have.text",
        "You have been selected to join “London Voice”!"
      );
      cy.get(mintingModulePopUp.mintingButton).should(
        "have.text",
        "Claim my VOICE ID"
      );
      cy.get(mintingModulePopUp.mintingModalLink)
        .should(
          "have.attr",
          "href",
          "https://ldnvoice.factorylabs.org/post/the-voting-system"
        )
        .and("have.attr", "target", "_blank")
        .and("have.text", "Learn more about how it works")
        .then(() => {
          //MINTING
          cy.get(mintingModulePopUp.mintingButton)
            .click()
            .wait(1000)
            .then(() => {
              cy.get(postMintScreen.postMintTitle).should("be.visible"); //wait for DOM to load after the mint
              cy.get(postMintScreen.postMintImage)
                .should("have.attr", "src")
                .then((src) => {
                  const id = +src.split("%2F")[6].split("&")[0];

                  //post minting screen content and styles check
                  cy.get(mintingModulePopUp.mintingModalContainer).should(
                    "be.visible"
                  );
                  cy.get(postMintScreen.postMintTitle).should(
                    "have.text",
                    `Hello LdnVoice Series 1 #${id}`
                  );
                  cy.get(postMintScreen.postMintMessage).should(
                    "have.text",
                    `You are now a member of the London Voice community. We will discuss, collaborate and reach consensus on the issues that you, the people of London, really care about.`
                  );
                  cy.get(mintingModulePopUp.mintingButton)
                    .should("be.visible")
                    .and("have.text", "Use my voice");

                  cy.get(mintingModulePopUp.mintingModalText).should(
                    "have.text",
                    `Congratulations! You are London VOICE ID #${id}`
                  );
                });
            });
        });
    });
  });
});
