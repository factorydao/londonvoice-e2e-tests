const {
  pageElements,
  iFrameElements,
} = require("../../pages/london-voice/mainPage");
import "cypress-real-events";


const getIframeDocument = (selector) => {
  return (
    cy
      .get(selector)
      // Cypress yields jQuery element, which has the real
      // DOM element under property "0".
      // From the real DOM iframe element we can get
      // the "document" element, it is stored in "contentDocument" property
      // Cypress "its" command can access deep properties using dot notation
      // https://on.cypress.io/its
      .its("0.contentDocument")
  );
};

const getIframeBody = (selector) => {
  // get the document
  return (
    getIframeDocument(selector)
      // automatically retries until body is loaded
      .its("body")
      .should("not.be.undefined")
      // wraps "body" DOM element to allow
      // chaining more Cypress commands, like ".find(...)"
      .then(cy.wrap)
  );
};

const getNestedIframeBody = (selector1, selector2) => {
  // get the document
  return (
    getIframeDocument(selector1)
      // automatically retries until body is loaded
      .its("body")
      .should("not.be.undefined")
      // wraps "body" DOM element to allow
      // chaining more Cypress commands, like ".find(...)"
      .then(cy.wrap)
      .then(($body) => {
        return cy
          .get($body)
          .find(selector2)
          .its("0.contentDocument")
          .find("body")
          .then(cy.wrap);
      })
  );
};

describe("Magic.Link", () => {
  before(function () {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.clearCookies("https://londongpt.vercel.app/");
    cy.clearAllLocalStorage();
    indexedDB.deleteDatabase("localforage");
    cy.visit("/");
    cy.confirmDisclaimer();

    return cy
      .mailslurp()
      .then((mailslurp) => mailslurp.createInbox())
      .then((inbox) => {
        // save inbox id and email address to this (make sure you use function and not arrow syntax)
        cy.wrap(inbox.id).as("inboxId");
        cy.wrap(inbox.emailAddress).as("emailAddress");
      });
  });

  it("Magic.Link Login and Disconnect", function () {
    // wait for ethers wallet to succesfully log in
    cy.intercept(
      "POST",
      "https://next.influencebackend.xyz/api/siwe/verify"
    ).as("verify");
    cy.wait("@verify");
    // now disconnect from the site to test magic.link
    cy.get(pageElements.profileButton).click();
    cy.get(pageElements.disconnectButton).click();

    // log in with magic.link
    cy.get(pageElements.connectButton)
      .click()
      .then(() => {
        // init temp mail client
        cy.log("Wrap inbox before test");
        return cy
          .mailslurp()
          .then((mailslurp) => mailslurp.createInbox())
          .then((inbox) => {
            cy.get(iFrameElements.loginEmailButton)
              .click()
              .wait(1000) // magic link iframe tends to appear slow
              .get("body")
              .then(($body) => {
                if ($body.find(iFrameElements.magicLinkOverlay).length) {
                  cy.get(iFrameElements.magicLinkConfirmationInput).type(
                    this.emailAddress
                  );
                  cy.get("button[type=submit]")
                    .click()
                    .then(async () => {
                      cy.log("Waiting for email");
                      await cy
                        .mailslurp()
                        .then((mailslurp) =>
                          mailslurp.waitForLatestEmail(
                            this.inboxId,
                            30000,
                            true
                          )
                        )
                        .then((email) => {
                          const text = email.textExcerpt;
                          const clearText = text.replace(/ /g, ""); //remove empty spaces
                          const codeString = "code:";
                          const codeLength = 6;
                          const OTPCode = clearText.substr(
                            clearText.indexOf(codeString) + codeString.length,
                            codeLength
                          );
                          console.log("OTP: ", OTPCode);
                          getIframeBody(".magic-iframe")
                            .find("#pin-code-input > div > div > input")
                            .eq(0)
                            .realType(OTPCode);
                        });
                    });
                }
              });
          });
      })
      .then(() => {
        // sign in with ethereum confirmation
        getIframeBody(".magic-iframe")
          .find(iFrameElements.SIWEConfirmation)
          .eq(1)
          .realClick();
      })
      // disconnect from the page
      .then(() => {
        cy.log("Successful connection");
        cy.get(pageElements.profileButton).click();

        cy.get(pageElements.disconnectButton).click();
      });
  });
});


