const { mainPageContent } = require("../../pages/london-voice/mainPage");
const {
  myLondonVoiceContainer,
} = require("../../pages/london-voice/myLondonVoice");

describe("My London Voice", () => {
  beforeEach("", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/");

    cy.confirmDisclaimer()

  });

  it("My London Voice content and styles check", () => {
    cy.get(mainPageContent.headerTab)
      .eq(1)
      .should("have.text", "My London Voice")
      .click();

    // nft image and title check
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");
    cy.get(myLondonVoiceContainer.idHeader).then((el) => {
      expect(el.text()).to.match(/Hello\s.*.#[0-9]/);
    });

    //Page description
    cy.get(myLondonVoiceContainer.MLVDescription).should(
      "have.text",
      "Choose from the activities below to contribute to the London Voice experiment"
    );

    //open activities styles check
    cy.get(myLondonVoiceContainer.activeContainer)
      .should("have.css", "background-color", "rgb(255, 255, 255)")
      .and("have.css", "box-shadow", "rgb(0, 0, 0) -4px 4px 0px 0px")
      .and("have.css", "border", "1px solid rgb(0, 0, 0)");

    // closed activities styles and content check
    cy.get(myLondonVoiceContainer.MLVClosedTitle).should(
      "have.text",
      "Closed activities"
    );
    cy.get(myLondonVoiceContainer.closedContainer)
      .should("have.css", "background-color", "rgba(0, 0, 0, 0.09)")
      .and("have.css", "box-shadow", "rgb(0, 0, 0) -4px 4px 0px 0px")
      .and("have.css", "border", "1px solid rgb(0, 0, 0)");

    // activity tags check
    const tags = {
      issue: "rgb(255, 108, 249)",
      vote: "rgb(102, 218, 255)",
      info: "rgb(255, 205, 30)",
    };
    cy.get(myLondonVoiceContainer.activityTag).then((activity) => {
      for (let i = 0; i < activity.length; i++) {
        if (activity[i].textContent === "issue") {
          expect(activity[i]).to.have.css("background-color", tags.issue);
        } else if (activity[i].textContent === "vote") {
          expect(activity[i]).to.have.css("background-color", tags.vote);
        } else if (activity[i].textContent === "info") {
          expect(activity[i]).to.have.css("background-color", tags.info);
        }
      }
    });

    // blog section content check
    cy.get(myLondonVoiceContainer.MLVBlog).should("have.text", "The BLOG");
    cy.get(myLondonVoiceContainer.learnMore)
      .should("have.attr", "target", "_blank")
      .and("have.attr", "href", "https://ldnvoice.factorylabs.org/blog");
  });
});
