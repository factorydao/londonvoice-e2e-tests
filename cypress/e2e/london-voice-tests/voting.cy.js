const { mainPageContent } = require("../../pages/london-voice/mainPage");
const {
  myLondonVoiceContainer,
  areYouSure,
  activityIssues,
  issueComponent,
  demographics,
} = require("../../pages/london-voice/myLondonVoice");
const { generate, count } = require("random-words");
require("cypress-real-events");

describe("'Are you sure' modal check", () => {
  beforeEach("visit my london voice tab", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/");

    cy.confirmDisclaimer()
    
    //stub proposals/vote before entering MLV page
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/spaces/test3/proposals?pageNumber=1&itemsPerPage=100",
      { fixture: "proposals" }
    ).as("proposals");
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/tasks/test3?pageNumber=1&itemsPerPage=100",
      { fixture: "question" }
    ).as("questions");
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/demographics/test3/0xd932de349A0e583b27CC98cC5025a4960C4dA5E1/7",
      { fixture: "demographics" }
    ).as("demographics");

    cy.get(mainPageContent.headerTab)
      .eq(1)
      .should("have.text", "My London Voice")
      .click();

    // demographics: https://next.influencebackend.xyz/api/demographics/test3/0xd932de349A0e583b27CC98cC5025a4960C4dA5E1/7
    // questions:    https://next.influencebackend.xyz/api/tasks/test3?pageNumber=1&itemsPerPage=100
    // questions1:   https://next.influencebackend.xyz/api/tasks/test3/Qmaki1HenCf52fVgdtV9oTrXtkz1jRkoRaQpeqgQ7egF9m
    // questions2:   https://next.influencebackend.xyz/api/tasks/test3/Qmaki1HenCf52fVgdtV9oTrXtkz1jRkoRaQpeqgQ7egF9m/check/0xd932de349a0e583b27cc98cc5025a4960c4da5e1
    // proposals:    https://next.influencebackend.xyz/api/spaces/test3/proposals?pageNumber=1&itemsPerPage=100
    // proposalVotes:   https://next.influencebackend.xyz/api/spaces/test3/proposal/QmNgMuu3CTbXbi9Ca91NR3Cbm8DhXDpkyHcCRshoLWgro7?address=0xd932de349a0e583b27cc98cc5025a4960c4da5e1&network=11155420
    // voting:(POST) https://next.influencebackend.xyz/api/votes
    // voting1:(GET) https://next.influencebackend.xyz/api/spaces/test3/proposal/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL?address=0xdb14b7afa4332fddfcfb7c8dd2d53c68306e0d69&network=11155420
    // demographics(GET): https://next.influencebackend.xyz/api/demographics/test3/0xd932de349A0e583b27CC98cC5025a4960C4dA5E1/*
  });

  it("Issue", () => {
    // wait for page to load properly
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");

    cy.get(myLondonVoiceContainer.activeContainer)
      .find(myLondonVoiceContainer.activityTag)
      .then((activity) => {
        const randomWord = generate({ exactly: 2 });
        for (let i = 0; i < activity.length; i++) {
          if (activity[i].textContent === "issue") {
            // stub open question
            cy.intercept(
              "GET",
              "https://next.influencebackend.xyz/api/tasks/test3/Qmaki1HenCf52fVgdtV9oTrXtkz1jRkoRaQpeqgQ7egF9m",
              { fixture: "questions1" }
            );
            cy.get(myLondonVoiceContainer.activeContainer).eq(i).click();
            cy.get(activityIssues.issueModal).should("be.visible");
            cy.get(issueComponent.submitButton).should("be.disabled");
            cy.get(issueComponent.inputNotActive)
              .first()
              .click()
              .then(() => {
                cy.get(issueComponent.inputTitle).type(randomWord[0]);
                cy.get(issueComponent.inputDescription).type(randomWord[1]);
                cy.get(issueComponent.submitButton)
                  .should("not.be.disabled")
                  .click();
                cy.get(areYouSure.areYouSureModal).should("be.visible");
                cy.get(areYouSure.areYouSureLeave)
                  .should("have.text", "go back")
                  .and("have.css", "background-color", "rgb(255, 108, 249)");
                // stub server request
                cy.intercept(
                  "POST",
                  "https://next.influencebackend.xyz/api/tasks/test3/Qmaki1HenCf52fVgdtV9oTrXtkz1jRkoRaQpeqgQ7egF9m/add-multiple-submissions",
                  { fixture: "addSubmission" }
                );
                cy.get(areYouSure.areYouSureGoBack)
                  .should("have.text", "Submit issues")
                  .and("have.css", "background-color", "rgb(195, 255, 98)")
                  .click();
                cy.get(issueComponent.myIssuesTab).should("be.visible");
                cy.get(issueComponent.submittedAnswerTitle)
                  .should("be.visible")
                  .and("have.text", randomWord[0]);
                cy.get(issueComponent.submittedAnswerDescription)
                  .should("be.visible")
                  .and("have.text", randomWord[1]);
                cy.get(issueComponent.submitButton)
                  .should("have.text", "Rank community issues")
                  .and("have.css", "background-color", "rgb(255, 108, 249)");
                cy.get(issueComponent.twitterButton).should(
                  "have.attr",
                  "target",
                  "_blank"
                );
              });
          }
        }
      });
  });
  it("Vote", () => {
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/proposals/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL",
      { fixture: "proposalSingle" }
    ).as("proposalSingle");
    // Stub vote power and empty ballot
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/spaces/test3/proposal/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL?address=0xd932de349a0e583b27cc98cc5025a4960c4da5e1&network=11155420",
      { fixture: "proposalVotes" }
    ).as("proposalVotes");
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/proposals/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL/vote-power/0xd932de349a0e583b27cc98cc5025a4960c4da5e1?identity=7&network=11155420",
      { fixture: "proposalScore" }
    ).as("proposalScore");
    // wait for proper page load
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");

    cy.get(myLondonVoiceContainer.activeContainer)
      .find(myLondonVoiceContainer.activityTag)
      .then((activity) => {
        for (let i = 0; i < activity.length; i++) {
          if (activity[i].textContent === "vote") {
            cy.get(myLondonVoiceContainer.activeContainer).eq(i).click();
            cy.get(activityIssues.voteContent).should("be.visible");
            cy.get('[class^="QuadraticVote_label"]').should(
              "have.text",
              "0/100 allocated"
            );

            cy.get(activityIssues.submitButtonVote).should("be.disabled");

            const value1 = 9;
            const value2 = 4;

            cy.get(".rc-slider-handle")
              .eq(0)
              .should("have.attr", "aria-valuenow", 0)
              .type("{rightarrow}".repeat(value1));

            cy.get(".rc-slider-handle")
              .eq(0)
              .should("have.attr", "aria-valuenow", value1);

            cy.get(".rc-slider-handle")
              .eq(1)
              .should("have.attr", "aria-valuenow", 0)
              .type("{rightarrow}".repeat(value2));

            cy.get(".rc-slider-handle")
              .eq(1)
              .should("have.attr", "aria-valuenow", value2);

            cy.get(activityIssues.submitButtonVote)
              .should("be.enabled")
              .click();
            cy.get(activityIssues.skipButtonVote).click();
            cy.get(activityIssues.skipButtonVote).click();

            cy.intercept(
              "GET",
              "https://next.influencebackend.xyz/api/spaces/test3/proposal/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL?address=0xd932de349a0e583b27cc98cc5025a4960c4da5e1&network=11155420",
              { fixture: "proposalScores" }
            ).as("proposalScores");

            cy.intercept(
              "POST",
              "https://next.influencebackend.xyz/api/votes",
              { fixture: "addQuestion" }
            ).as("addQuestion");

            cy.get(activityIssues.finalSubmitButtonVote).click();
            cy.get(activityIssues.infoRow).should("be.visible");
            cy.get(activityIssues.infoTag)
              .should("have.attr", "target", "_blank")
              .and(
                "have.attr",
                "href",
                "https://factorydao.notion.site/London-VOICE-app-e4abd8314d0e4dc8977c6fd45b9cfa89"
              );

            cy.get(activityIssues.successContainer).should(
              "have.css",
              "background-color",
              "rgb(195, 255, 98)"
            );
            cy.get(activityIssues.successContainerText).should(
              "have.text",
              "VOTE SUBMITTED"
            );
            cy.get(activityIssues.scoreBox).eq(0).should("have.text", value1);
            cy.get(activityIssues.scoreBox).eq(1).should("have.text", value2);
            cy.get(activityIssues.votedImage).should("be.visible");

            cy.get(activityIssues.opinionsTab).click();
            cy.get(activityIssues.opinionSingle).should("have.length", 2);
            cy.get(activityIssues.opinionHeader)
              .eq(0)
              .should("have.text", "Test3 #7");
            cy.get(activityIssues.opinionHeader)
              .eq(1)
              .should("have.text", "Test3 #7");
            cy.get(activityIssues.opinionCoin)
              .eq(0)
              .should("have.text", `${value1} Votes`);
            cy.get(activityIssues.opinionCoin)
              .eq(1)
              .should("have.text", `${value2} Votes`);

            cy.get(activityIssues.resultsTab).click();
            cy.get(activityIssues.scoreBox).eq(0).should("have.text", value1);
            cy.get(activityIssues.scoreBox).eq(1).should("have.text", value2);
            cy.get(activityIssues.influenceContainerResultsLabel).then(
              (label) => {
                expect(label[0]).to.have.text("Total Votes:");
                expect(label[1]).to.have.text("Total Vote Credits: ");
                expect(label[2]).to.have.text("Total Voter: ");
              }
            );
            cy.get(activityIssues.influenceContainerResultsValue).then(
              (value) => {
                expect(value[0]).to.have.text(13);
                expect(value[1]).to.have.text(97);
                expect(value[2]).to.have.text(1);
              }
            );
            cy.get(activityIssues.influenceContainerPieChart).should(
              "be.visible"
            );
          }
        }
      });
  });
  it("Info", () => {
    // wait for proper page load
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");

    cy.intercept("POST", "https://next.influencebackend.xyz/api/demographics", {
      fixture: "demographicsAnswers",
    }).as('demographicAnswer')

    cy.get(myLondonVoiceContainer.activeContainer)
      .find(myLondonVoiceContainer.activityTag)
      .then((activity) => {
        for (let i = 0; i < activity.length; i++) {
          if (activity[i].textContent === "info") {
            cy.get(myLondonVoiceContainer.activeContainer).eq(i).click();
            cy.get(demographics.questionContainer).should("be.visible");
            for (let i = 0; i < 9; i++) {
              cy.get(demographics.submitQuestionButton).should("be.disabled");
              cy.get(demographics.radioButton).eq(0).check();
              cy.get(demographics.submitQuestionButton).then(button => {
                expect(button).not.to.be.disabled
                if( i==8){
                  expect(button).to.have.text('complete')
                }else{
                  expect(button).to.have.text('Next')
                }
              })
                .click();
            }
          }
        }
      });
  });
});
