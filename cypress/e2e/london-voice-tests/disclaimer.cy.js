const { pageElements } = require("../../pages/london-voice/mainPage");

describe("Disclaimer", () => {
  beforeEach(() => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/");
    indexedDB.deleteDatabase("localforage");
  });
  it("Disclaimer content and styles check. Check if it closes properly", () => {

    cy.get(pageElements.disclaimerModal).should("be.visible");
    cy.get(pageElements.disclaimerModalHeader).should(
      "have.text",
      "Disclaimer"
    );
    cy.get(pageElements.disclaimerModalContentSubtitle).then((div) => {
      expect(div[0]).to.have.text("Welcome to the London Voice App!");
      expect(div[1]).to.have.text(
        "Before you dive in, please take a moment to review our terms:"
      );
    });
    cy.get(pageElements.disclaimerModalContent).should(
      "have.text",
      "Data Usage: Your participation and the data you provide will be used to enhance community decision-making processes. By continuing, you agree to our Privacy Policy and how we handle your information.Content Sharing: Contributions made within the app may be used in discussions, reports, and promotions related to London Voice activities. No personal identifiable information will be disclosed in these activities. All efforts will be made to ensure privacy and respect for all users.Recording Consent: Portions of our events and activities may be recorded for archival and promotional purposes. Direct recordings of app interactions for promotional purposes will only be made with explicit user consent.Community Standards: We are committed to maintaining a respectful and inclusive environment. Please adhere to our community guidelines at all times."
    );

    cy.get(pageElements.disclaimerModalContentLink).then((a) => {
      expect(a).to.have.attr("target", "_blank");
      expect(a[0]).to.have.attr(
        "href",
        "https://ldnvoice.factorylabs.org/privacy-policy-app"
      );
      expect(a[1]).to.have.attr(
        "href",
        "https://ldnvoice.factorylabs.org/community-guidelines"
      );
    });

    cy.get(pageElements.disclaimerModalContentSummary).should(
      "have.text",
      "By clicking `Agree` you acknowledge that you understand and consent to these terms. If you do not agree, you may discontinue use of the app."
    );

    cy.get(pageElements.disclaimerButton)
      .should("have.text", "agree & continue")
      .and("have.css", "background-color", "rgb(195, 255, 98)");
    
    //close disclaimer
    cy.get(pageElements.disclaimerButton).click()
    cy.get(pageElements.disclaimerModal).should("not.exist")

  });
});
