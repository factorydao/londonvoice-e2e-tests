const { mainPageContent } = require("../../pages/london-voice/mainPage");
const {
  myLondonVoiceContainer,
  areYouSure,
  activityIssues,
  demographics
} = require("../../pages/london-voice/myLondonVoice");

describe("'Are you sure' modal check", () => {
  beforeEach("visit my london voice tab", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/");
    cy.confirmDisclaimer()

    //stub proposals/vote before entering MLV page
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/spaces/test3/proposals?pageNumber=1&itemsPerPage=100",
      { fixture: "proposals" }
    ).as("proposals");
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/tasks/test3?pageNumber=1&itemsPerPage=100",
      { fixture: "question" }
    ).as("questions");
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/demographics/test3/0xd932de349A0e583b27CC98cC5025a4960C4dA5E1/7",
      { fixture: "demographics" }
    ).as("demographics");

    cy.get(mainPageContent.headerTab)
      .eq(1)
      .should("have.text", "My London Voice")
      .click();



    
  });

  it("Issue", () => {
    // wait for proper page load
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");

    cy.get(myLondonVoiceContainer.activeContainer)
      .find(myLondonVoiceContainer.activityTag)
      .then((activity) => {
        for (let i = 0; i < activity.length; i++) {
          if (activity[i].textContent === "issue") {
            // stub empty question to test Are You Sure screen
            cy.intercept(
              "GET",
              "https://next.influencebackend.xyz/api/tasks/test3/Qmaki1HenCf52fVgdtV9oTrXtkz1jRkoRaQpeqgQ7egF9m",
              { fixture: "questions1" }
            );
            cy.get(myLondonVoiceContainer.activeContainer).eq(i).click();
            cy.get(activityIssues.issueModal).should("be.visible");
          }
        }
      })
      .then(() => {
        // check modal styles
        // check going back to form button
        // check leaving form button
        cy.checkAreYouSureModal("issue");
      });
  });
  it("Vote", () => {
    // wait for proper page load
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/proposals/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL",
      { fixture: "proposalSingle" }
    ).as("proposalSingle");
    // Stub vote power and empty ballot
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/spaces/test3/proposal/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL?address=0xd932de349a0e583b27cc98cc5025a4960c4da5e1&network=11155420",
      { fixture: "proposalVotes" }
    ).as("proposalVotes");
    cy.intercept(
      "GET",
      "https://next.influencebackend.xyz/api/proposals/QmaJDHKmFUG6TPdeETZTE93kwdxsB1j89rpczfJo2CbuRL/vote-power/0xd932de349a0e583b27cc98cc5025a4960c4da5e1?identity=7&network=11155420",
      { fixture: "proposalScore" }
    ).as("proposalScore");
    // wait for proper page load
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");

    cy.get(myLondonVoiceContainer.activeContainer)
      .find(myLondonVoiceContainer.activityTag)
      .then((activity) => {
        for (let i = 0; i < activity.length; i++) {
          if (activity[i].textContent === "vote") {
            cy.get(myLondonVoiceContainer.activeContainer).eq(i).click();
            cy.get(activityIssues.voteContent).should("be.visible");
            cy.get('[class^="QuadraticVote_label"]').should(
              "have.text",
              "0/100 allocated"
            );

            cy.get(activityIssues.submitButtonVote).should("be.disabled");
          }
        }
      })
      .then(() => {
        // check modal styles
        // check going back to form button
        // check leaving form button
        cy.checkAreYouSureModal("vote");
      });
  });
  it("Info", () => {
    // wait for proper page load
    cy.get(myLondonVoiceContainer.idImage).should("be.visible");

    cy.get(myLondonVoiceContainer.activeContainer)
      .find(myLondonVoiceContainer.activityTag)
      .then((activity) => {
        for (let i = 0; i < activity.length; i++) {
          if (activity[i].textContent === "info") {
            cy.get(myLondonVoiceContainer.activeContainer).eq(i).click();
            cy.get(demographics.questionContainer).should("be.visible");
          }
        }
      })
      .then(() => {
        // check modal styles
        // check going back to form button
        // check leaving form button
        cy.checkAreYouSureModal("info");
      });
  });
});
