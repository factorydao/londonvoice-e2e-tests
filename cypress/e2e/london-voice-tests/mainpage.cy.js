const {
  pageElements,
  mainPageContent,
  iFrameElements,
} = require("../../pages/london-voice/mainPage");

describe("Main Page basic checks", () => {
  beforeEach("", () => {
    cy.intercept({ resourceType: /xhr|fetch/ }, { log: false });
    cy.visit("/");

    cy.confirmDisclaimer()
  });
  it("Main Page content and styles check", () => {
    cy.get(mainPageContent.container.paragraph).then((paragraph) => {
      expect(paragraph[0]).to.have.text(
        "A democratically built AI, trained by the real opinions of real London citizens, designed to serve as a collective voice for the community."
      );
      expect(paragraph[1]).to.have.text(
        "When was the last time you were asked your opinion on the issues YOU care about?London Voice synthesises human voices and technology, using the latest emerging technologies and participatory research methodologies to build a data set of sense-making and dialogue that we think could inform civic decision makers of all kinds.Join us in our experiment and help us build a better, more democratic society."
      );
      expect(paragraph[2]).to.have.text(
        "Your Data is Yours and it's Valuable.All of the data entered into the London VOICE web app should be considered “open source” with the understanding that no personal identifying information is kept by our system. We will only know you by your Voting ID. We may ask for your demographic data, which will improve the scientific outcomes of this experiment, but it is your choice whether you provide it.You can request to remove your data from the experiment. Your privacy, your data & your rights are of paramount importance to us.PRIVACY POLICY"
      );
    });

    cy.get(mainPageContent.container.blackSection).each((blackSection) => {
      expect(blackSection).to.have.css("background-color", "rgb(0, 0, 0)");
    });

    cy.get(mainPageContent.container.blackSectionHeader).then((header) => {
      expect(header).to.have.text("What does  London Voice do?");
      expect(header).to.have.css("color", "rgb(195, 255, 98)");
    });
    cy.get(mainPageContent.container.blackSectionDoings).then((doing) => {
      expect(doing).to.have.css("border-left", "1px solid rgb(255, 255, 255)");
      expect(doing[0]).to.have.text(
        "Understands and represents the diverse voices of London"
      );
      expect(doing[1]).to.have.text(
        "Makes proposals for improvements based on solutions people have suggested"
      );
      expect(doing[2]).to.have.text(
        "Allows people to own, control and monetise their data"
      );
      expect(doing[3]).to.have.text(
        "Provides deep insights into issues that people really care about"
      );
    });

    cy.get(mainPageContent.container.learnMore).then((learnMore) => {
      expect(learnMore).to.have.attr("target", "_blank");
      expect(learnMore).to.have.attr(
        "href",
        "https://ldnvoice.factorylabs.org/blog"
      );
    });
    cy.get(mainPageContent.container.privacyPolicy).then((privacyPolicy) => {
      expect(privacyPolicy).to.have.attr("target", "_blank");
      expect(privacyPolicy).to.have.attr(
        "href",
        "https://ldnvoice.factorylabs.org/privacy-policy-app"
      );
    });
    cy.get(mainPageContent.container.footer).then((footer) => {
      expect(footer).to.have.text(
        " “London Voice” is a democratic AI experiment facilitated by FactoryLabs 2024"
      );
    });
  });

  it("Header tabs check. Logged in and logged out.", () => {
    // wait for ethers wallet to succesfully log in
    cy.intercept(
      "POST",
      "https://next.influencebackend.xyz/api/siwe/verify"
    ).as("verify");
    cy.wait("@verify");

    cy.get(mainPageContent.headerTab).then((tabs) => {
      expect(tabs.length).to.eq(4);
      expect(tabs[0]).to.have.text("Home");
      expect(tabs[1]).to.have.text("My London Voice");
      expect(tabs[2]).to.have.text("How it works");
      expect(tabs[3]).to.have.text("Contact us");
      expect(tabs[0]).to.have.attr("target", "_self");
      expect(tabs[0]).to.have.attr("href", "/");
      expect(tabs[1]).to.have.attr("target", "_self");
      expect(tabs[1]).to.have.attr("href", "/my-london-voice");
      expect(tabs[2]).to.have.attr("target", "_blank");
      expect(tabs[2]).to.have.attr(
        "href",
        "https://factorydao.notion.site/London-VOICE-app-e4abd8314d0e4dc8977c6fd45b9cfa89"
      );
      expect(tabs[3]).to.have.attr("target", "_blank");
      expect(tabs[3]).to.have.attr("href", "https://t.me/factdao");
    });

    // now disconnect from the site to test magic.link
    cy.get(pageElements.profileButton).click();
    cy.get(pageElements.disconnectButton).click();

    cy.get(mainPageContent.headerTab).then((tabs) => {
      expect(tabs.length).to.eq(3);
      expect(tabs[0]).to.have.text("Home");
      expect(tabs[1]).to.have.text("How it works");
      expect(tabs[2]).to.have.text("Contact us");
    });
  });

  it("Login modal at the bottom check. Styles and content.", () => {
    // wait for ethers wallet to succesfully log in
    cy.intercept(
      "POST",
      "https://next.influencebackend.xyz/api/siwe/verify"
    ).as("verify");
    cy.wait("@verify");
    cy.get(pageElements.profileButton).click();
    cy.get(pageElements.disconnectButton)
      .click()
      //check if logged in
      .then(() => {
        cy.get(mainPageContent.loginModalBottomPage)
          .should("be.visible")
          .then(() => {
            cy.get(mainPageContent.loginModalBottomPageLeftMargin)
              .should("be.visible")
              .and("have.css", "border-right", "8px solid rgb(0, 0, 0)")
              .and("have.css", "border-top", "8px solid rgba(0, 0, 0, 0)");

            cy.get(mainPageContent.loginModalBottomPageText).should(
              "have.text",
              "Already have an account?"
            );

            cy.get(mainPageContent.loginModalBottomPageLoginButton)
              .should("have.css", "background-color", "rgb(195, 255, 98)")
              .and("have.text", "Login / connect");

            cy.get(mainPageContent.loginModalBottomPageLink)
              .should("have.text", "Learn more about how it works")
              .and("have.attr", "target", "_blank")
              .and(
                "have.attr",
                "href",
                "https://ldnvoice.factorylabs.org/post/the-voting-system"
              );
          });
      });
  });
});
