// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import "./commands";
// Alternatively you can use CommonJS syntax:
// // require('./commands')
import "cypress-mailslurp";

import { Wallet } from "ethers";
import { personalSign } from "@metamask/eth-sig-util";

// inject ethers wallet
Cypress.on("window:before:load", async (win) => {
  const wallet = new Wallet(Cypress.env("PRIV_KEY"));
  const address = await wallet.getAddress();
  win.ethereum = {
    on: (eventName, callback) => {
      if (eventName === "accountsChanged") {
        callback([address]);
      }
    },
    request: async (payload) => {
      if (payload.method === "eth_requestAccounts") {
        return Promise.resolve([address]);
      }
      if (payload.method === "eth_chainId") {
        return Promise.resolve("0x1");
      }
      if (payload.method === "personal_sign") {
        const signature = personalSign({
          privateKey: Buffer.from(wallet.privateKey.slice(2), "hex"),
          data: payload.params[0],
        });
        return signature;
      }
      return Promise.resolve([address]);
    },
    isMetaMask: true,
  };
});
