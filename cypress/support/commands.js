const {
  areYouSure,
  activityIssues,
} = require("../pages/london-voice/myLondonVoice");
const {
  pageElements,
} = require("../pages/london-voice/mainPage");

Cypress.Commands.add("confirmDisclaimer", () => {
  cy.get("main").then(($div) => {
    if ($div.find(pageElements.disclaimerButton).length) {
      cy.get(pageElements.disclaimerButton).click();
    }
  });
});

Cypress.Commands.add("checkAreYouSureModal", (activityType) => {
  const description = {
    issue:
      "If you leave this page without submitting your issues you will loose all your progress",
    issuePartial:
      "You have not used all your issue boxes, once you click submit your issues you will not be able to come back later to use them",
    vote: "If you leave this page without submitting your vote you will loose all your progress",
    info: "If you leave this page without finishing the questionnaire you will loose your progress",
  };

  cy.get(activityIssues.closeModal).click();
  cy.get(areYouSure.areYouSureModal).should("be.visible");
  cy.get(areYouSure.areYouSureIcon).should("be.visible");
  cy.get(areYouSure.areYouSureDescription).should(
    "have.text",
    description[activityType]
  );
  cy.get(areYouSure.areYouSureLeave)
    .should("have.css", "background-color", "rgb(249, 65, 65)")
    .and("have.text", "leave");
  if (activityType === "issuePartial") {
    cy.get(areYouSure.areYouSureGoBack)
      .should("have.css", "background-color", "rgb(255, 108, 249)")
      .and("have.text", "go back")
      .click();
  } else {
    cy.get(areYouSure.areYouSureGoBack)
      .should("have.css", "background-color", "rgb(195, 255, 98)")
      .and("have.text", "go back")
      .click();
  }
  // check going back to form
  cy.get(areYouSure.areYouSureModal).should("not.exist");
  cy.get(activityIssues.activityModalOpen).should("be.visible");
  // check leaving the form
  cy.get(activityIssues.closeModal).click();
  cy.get(areYouSure.areYouSureModal).should("be.visible");
  cy.get(areYouSure.areYouSureLeave).click();
  cy.get(areYouSure.areYouSureModal).should("not.exist");
  cy.get(activityIssues.activityModalOpen).should("not.exist");
});
 