const webpack = require('@cypress/webpack-preprocessor');

module.exports = async (on, config) => {
  const options = {
    webpackOptions: {
      resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
      module: {
        rules: [
          {
            test: /\.m?js$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            }
          }
        ]
      }
    },
  };

  on('file:preprocessor', webpack(options));

  // important: return the changed config
  return config
}