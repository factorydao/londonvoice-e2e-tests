const mintingButton = '[class^="Button_button__efuko"]';
const mintingModalContainer = '[class^="Modal_container_"]';
const mintingModalText = '[class^="ActionVoiceModal_text"]';
const mintingModalLink = '[class^="ActionVoiceModal_container"] > a';

module.exports.mintingModulePopUp = {
  mintingButton,
  mintingModalContainer,
  mintingModalText,
  mintingModalLink
};

const postMintTitle = '[class^="ClaimVoice_successHeader"] > div'
const postMintMessage = '[class^="ClaimVoice_successInfo"]'
const postMintImage = '[class^="ClaimVoice_nftImageContainer"] > img'
const postMintContainer = '[class^="ClaimVoice_container"]'

module.exports.postMintScreen = {
    postMintTitle,
    postMintMessage,
    postMintImage,
    postMintContainer
}