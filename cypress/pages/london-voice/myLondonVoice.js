const idImage= '[class^="MyLondonVoice_heading"] > img'
const idHeader= '[class^="MyLondonVoice_heading"] > h1'

const MLVDescription= '[class^="MyLondonVoice_description"]'

const MLVActivities = '[class^="MyLondonVoice_activities"]'
const MLVClosed = '[class^="MyLondonVoice_closed"]'
const MLVClosedTitle = '[class^="MyLondonVoice_closed"] > h2'
const MLVBlog = '[class^="MyLondonVoice_title"]'
const activityTag = '[class^="Activity_text"]'
const activeContainer = '[class^="MyLondonVoice_container"] > [class^="MyLondonVoice_activities"] > [class^="Activity_container"]'
const closedContainer = '[class^="MyLondonVoice_closed"] > [class^="MyLondonVoice_activities"] > [class^="Activity_container"]'
const learnMore = '[class^="LearnMore_container"] > a'

module.exports.myLondonVoiceContainer = {
    idImage,
    idHeader,
    MLVDescription,
    MLVActivities,
    MLVClosed,
    MLVClosedTitle,
    MLVBlog,
    activityTag,
    activeContainer,
    closedContainer,
    learnMore
}


const areYouSureModal = '[class^="AreYouSure_container"]'
const areYouSureIcon = '[class^="AreYouSure_container"] > img'
const areYouSureDescription = '[class^="AreYouSure_description"]'
const areYouSureLeave = '[class^="AreYouSure_buttons"] > button:nth-child(1)'
const areYouSureGoBack = '[class^="AreYouSure_buttons"] > button:nth-child(2)'

module.exports.areYouSure = {
    areYouSureModal,
    areYouSureIcon,
    areYouSureDescription,
    areYouSureLeave,
    areYouSureGoBack
}

const activityModalOpen = '[class^="MyLondonVoice_modalOpen"]'
const closeModal = '[class^="Modal_content"] > img'
const issueModal = '[class^="Issue_modal"]'
const voteModal = '[class^="Vote_modal"]'
const voteContent = '[class^="Vote_content"]'
const voteBallot = '[class^="VoteComponent_ballot"]'
const infoModal = '[class^="Info_modal"]'
const submitButtonVote = '[class^="QuadraticVote_footer"] > button'
const skipButtonVote = '[class^="OpinionChat_next"] > span'
const finalSubmitButtonVote = '[class^="SubmitVote_footer"] > button'
const successContainer = '[class^="QuadraticVote_success"]'
const successContainerText = '[class^="QuadraticVote_success"] > strong'
const scoreBox = '[class^="QuadraticVote_box"] > [class^="QuadraticVote_disabled"] > span'
const votedImage = '[class^="VoteComponent_ballot"] > img'
const infoRow = '[class^="QuadraticVote_info"]'
const opinionsTab = '[class^="Vote_tab"]:nth-child(2)'
const opinionSingle = '[class^="Opinions_item"]'
const opinionHeader = '[class^="Opinions_header"] > span'
const opinionTag = '[class^="Opinions_tag"]'
const opinionCoin = '[class^="Opinions_coin"]'
const opinionDescription = '[class^="Opinions_text"]'
const resultsTab = '[class^="Vote_tab"]:nth-child(3)'
const influenceContainerResultsLabel = '[class^="MyInfluence_data"] > [class^="MyInfluence_item"] > [class^="MyInfluence_label"]'
const influenceContainerResultsValue = '[class^="MyInfluence_data"] > [class^="MyInfluence_item"] > [class^="MyInfluence_value"]'
const influenceContainerPieChart = 'canvas'
const infoTag = '[class^="Vote_navBar"] > a'


module.exports.activityIssues = {
    activityModalOpen,
    closeModal,
    issueModal,
    voteModal,
    voteContent,
    voteBallot,
    infoModal,
    submitButtonVote,
    skipButtonVote,
    finalSubmitButtonVote,
    successContainer,
    successContainerText,
    scoreBox,
    votedImage,
    infoRow,
    opinionsTab,
    opinionSingle,
    opinionHeader,
    opinionTag,
    opinionCoin,
    opinionDescription,
    resultsTab,
    influenceContainerResultsLabel,
    influenceContainerResultsValue,
    influenceContainerPieChart,
    infoTag
}

const inputNotActive = '[class^="IssuesInput_container"] > [class^="IssuesInput_notActive"]'
const inputTitle = '[class^="IssuesInput_content"] > input'
const inputDescription = '[class^="IssuesInput_content"] > textarea'
const submitButton = '[class^="MyIssuesForm_container"] > button'
const myIssuesTab = '[class^="Issue_tabs"] > button > span:nth-child(1)'
const submittedAnswerTitle = '[class^="SingleIssuePreview_title"]'
const submittedAnswerDescription = '[class^="SingleIssuePreview_description"]'
const twitterButton = '[class^="SingleIssuePreview_header"] > form'

module.exports.issueComponent = {
    inputNotActive,
    submitButton,
    inputTitle,
    inputDescription,
    myIssuesTab,
    submittedAnswerTitle,
    submittedAnswerDescription,
    twitterButton

}

const questionContainer = '[class^="InfoQuestion_container"]'
const submitQuestionButton = '[class^="InfoQuestion_container"] > button'
const radioButton = '[class^="InfoQuestion_answers"] > label > input'

module.exports.demographics = {
    questionContainer,
    submitQuestionButton,
    radioButton
}