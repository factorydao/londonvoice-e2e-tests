const disclaimerModal = '[class^="DisclaimerModal_disclaimerForm"]';
const disclaimerModalHeader = '[class^="DisclaimerModal_headerText"]';
const disclaimerModalContentSubtitle = '[class^="DisclaimerModal_modalBody"] > div';
const disclaimerModalContent = '[class^="DisclaimerModal_points"]';
const disclaimerModalContentLink = '[class^="DisclaimerModal_points"] > ol > li > a';
const disclaimerModalContentSummary = '[class^="DisclaimerModal_summary"]';
const disclaimerButton = '[class^="DisclaimerModal_paragraph"] > button'

const connectButton = '#connect-button'
const profileButton = 'button > img'
const disconnectButton = '[data-testid="rk-disconnect-button"]'

module.exports.pageElements = {
  disclaimerModal,
  disclaimerModalHeader,
  disclaimerModalContentSubtitle,
  disclaimerModalContent,
  disclaimerModalContentLink,
  disclaimerModalContentSummary,
  disclaimerButton,
  connectButton,
  profileButton,
  disconnectButton
};
const loginEmailButton = '[class^="ConnectorsModal_modalBody"] > button'
const magicLinkOverlay = '.Magic__formOverlay'
const magicLinkConfirmationInput = '#MagicFormInput'
// const SIWEConfirmation = '#modal-portal > div:nth-child(2) > div > div > .mg_bw.mg_by > button > span'
const SIWEConfirmation = 'div > button > div > span'
const SIWEContainer = '[class^="Siwe_container"]'
const loginMetamaskButton = '[class^="ConnectorsModal_otherWalletContainer"] > div:nth-child(1)'
const loginModal = '[class^="ConnectorsModal_container"]'
const loginModalClose = '[class^="ConnectorsModal_container"] > img'

module.exports.iFrameElements = {
  loginEmailButton,
  magicLinkOverlay,
  magicLinkConfirmationInput,
  SIWEConfirmation,
  SIWEContainer,
  loginMetamaskButton,
  loginModal,
  loginModalClose
}

const headerTab = '[class^="NavBar_menuItems"] > a'
const mainContainer = '[class^="Home_container"]'
const container = {
  paragraph: `${mainContainer} > [class^="Home_paragraph"]`,
  privacyPolicy: `${mainContainer} > [class^="Home_paragraph"] > a`,
  blackSection: `${mainContainer} > [class^="Home_blackSection"]`,
  blackSectionHeader: `${mainContainer} > [class^="Home_blackSection"] > h1`,
  blackSectionDoings: `${mainContainer} > [class^="Home_blackSection"] > [class^="Home_doings"] > span`,
  learnMore: `${mainContainer} > [class^="Home_learnMore"] > [class^="LearnMore_container"] > a`,
  footer: `${mainContainer} > [class^="Home_footer"]`
}
const loginModalBottomPage = '[class^="Modal_content"]'
const loginModalBottomPageLeftMargin = '[class^="Modal_content"] > [class^="Modal_leftMargin"]'
const loginModalBottomPageText = '[class^="Home_text"]'
const loginModalBottomPageLoginButton = '[class^="Button_button"]'
const loginModalBottomPageLink = '[class^="Home_modal"] > a'


module.exports.mainPageContent = {
  headerTab,
  container,
  loginModalBottomPage,
  loginModalBottomPageLeftMargin,
  loginModalBottomPageText,
  loginModalBottomPageLoginButton,
  loginModalBottomPageLink
}