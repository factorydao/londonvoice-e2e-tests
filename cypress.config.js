const { defineConfig } = require("cypress");
const fs = require("fs");

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = defineConfig({
  chromeWebSecurity: false,
  viewportHeight: 1080,
  viewportWidth: 1920,
  pageLoadTimeout: 40000,
  defaultCommandTimeout: 30000,
  responseTimeout: 30000,
  requestTimeout: 30000,
  video: false,
  retries: {
    runMode: 2,
    openMode: 1,
  },
  reporter: "mochawesome",
  reporterOptions: {
    reportDir: 'cypress/results',
    overwrite: false,
    html: false,
    json: true,
  },
  reporterEnabled: "spec, mocha-junit-reporter",
  mochaJunitReporterReporterOptions: {
    mochaFile: "cypress/results/results-[hash].xml",
  },
  
  e2e: {
    async setupNodeEvents(on, config) {
      on("task", {
        // fs
        getQRCodes: async () => {
          const splitQRCodeLink = (qrCode, codeString, codeLength, i) => {
            const codeStringF = codeString;
            const codeLengthF = codeLength;
            const splitText = qrCode.substr(
              qrCode.indexOf(codeStringF) + codeStringF.length,
              codeLengthF
            );
            return splitText;
          };

          const file1 = await fs.readFileSync(
            "./cypress/fixtures/qrCodes.json",
            {
              encoding: "utf8",
            }
          );
          const qrCodes = JSON.parse(file1).qrCodeLinks;

          for (let i = 0; i < qrCodes.length; i++) {
            const qrCode = qrCodes[i];
            const code = splitQRCodeLink(qrCode, "code=", 64, i);
            const realm = splitQRCodeLink(qrCode, "groupKey=", 5, i);
            const response = await fetch(
              `https://minting-next.influencebackend.xyz/minting/${realm}/check/0xd932de349A0e583b27CC98cC5025a4960C4dA5E1?code=${code}`
            );
            const res = await response.json();
            if (res.length) {
              return qrCode;
            }
          }

          return null;
        },
      });

      return config;
    },
    baseUrl: "https://londongpt-git-marta-test-fvt.vercel.app/",
    env: {
      MAILSLURP_API_KEY:
        "e8928b9ebbc3db8078ab6154ca0f4d6b447a4edc09c9f0caa750f4c24fb65468",
    },
  },
});
